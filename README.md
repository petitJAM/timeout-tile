# Timeout Tile

An app for quickly changing your screen timeout setting with a tap of a quick tile.

## License
You can download, build, and use the app, that's fine. Don't publish this code or any build outputs anywhere.
