package com.alexpetitjean.timeouttile

import android.app.Application
import timber.log.Timber

class TimeoutTileApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initTimber()
    }

    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }
}
