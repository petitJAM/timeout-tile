package com.alexpetitjean.timeouttile

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

val toggleStateKey = booleanPreferencesKey("toggle_state")
val originalTimeoutValueKey = intPreferencesKey("original_timeout_value")
val temporaryTimeoutValueKey = intPreferencesKey("temporary_timeout_value")

const val DEFAULT_TEMPORARY_TIMEOUT = 600_000 // 10 minutes
