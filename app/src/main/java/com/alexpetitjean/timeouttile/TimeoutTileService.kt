package com.alexpetitjean.timeouttile

import android.provider.Settings
import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class TimeoutTileService : TileService() {

    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    override fun onCreate() {
        super.onCreate()
        scope.launch {
            dataStore.data
                .map { prefs ->
                    val toggleState = prefs[toggleStateKey] ?: false

                    updateTileState(toggleState)

                    if (toggleState) {
                        prefs[temporaryTimeoutValueKey] ?: DEFAULT_TEMPORARY_TIMEOUT
                    } else {
                        prefs[originalTimeoutValueKey]
                            ?: getCurrentTimeout()
                    }
                }
                .collect { timeout ->
                    Settings.System.putInt(
                        contentResolver,
                        Settings.System.SCREEN_OFF_TIMEOUT,
                        timeout
                    )
                }
        }
    }

    override fun onStartListening() {
        super.onStartListening()

        val toggleState = runBlocking {
            dataStore.data.first()[toggleStateKey] ?: false
        }
        updateTileState(toggleState)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun onClick() {
        super.onClick()
        scope.launch {
            dataStore.edit { prefs ->
                val toggleState = prefs[toggleStateKey] ?: false
                prefs[toggleStateKey] = !toggleState
            }
        }
    }

    private fun updateTileState(toggleState: Boolean) {
        val hasPermission = Settings.System.canWrite(applicationContext)
        qsTile.state = if (!hasPermission) {
            Tile.STATE_UNAVAILABLE
        } else if (toggleState) {
            Tile.STATE_ACTIVE
        } else {
            Tile.STATE_INACTIVE
        }
        qsTile.updateTile()
    }

    private fun getCurrentTimeout() =
        Settings.System.getInt(
            contentResolver,
            Settings.System.SCREEN_OFF_TIMEOUT
        )
}
