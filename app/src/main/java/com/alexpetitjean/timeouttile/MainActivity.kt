package com.alexpetitjean.timeouttile

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import com.alexpetitjean.timeouttile.ui.main.MainScreen
import com.alexpetitjean.timeouttile.ui.theme.TimeoutTileTheme
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import timber.log.Timber

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TimeoutTileTheme {
                val systemUiController = rememberSystemUiController()
                val useDarkIcons = MaterialTheme.colors.isLight

                SideEffect {
                    systemUiController.setSystemBarsColor(
                        color = Color.Transparent,
                        darkIcons = useDarkIcons
                    )
                }

                Surface(color = MaterialTheme.colors.background) {
                    MainScreen(
                        dataStore = dataStore,
                        launchPermissionManagement = this::launchPermissionManagement,
                        canWrite = { Settings.System.canWrite(this) },
                        getCurrentTimeoutValue = this::getCurrentTimeoutValue,
                    )
                }
            }
        }
    }

    private fun launchPermissionManagement() {
        val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
        startActivity(intent)
    }

    private fun getCurrentTimeoutValue(): Int =
        Settings.System.getInt(
            contentResolver,
            Settings.System.SCREEN_OFF_TIMEOUT,
            -1
        )
}
