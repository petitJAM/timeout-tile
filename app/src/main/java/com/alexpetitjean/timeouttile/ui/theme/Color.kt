package com.alexpetitjean.timeouttile.ui.theme

import androidx.compose.ui.graphics.Color

val Blue400 = Color(0xFF43A7F8)
val Blue700 = Color(0xFF1B78D6)
val Blue900 = Color(0xFF1049A5)
