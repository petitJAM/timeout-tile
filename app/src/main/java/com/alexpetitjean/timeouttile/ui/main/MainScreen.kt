package com.alexpetitjean.timeouttile.ui.main

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.alexpetitjean.timeouttile.DEFAULT_TEMPORARY_TIMEOUT
import com.alexpetitjean.timeouttile.R
import com.alexpetitjean.timeouttile.originalTimeoutValueKey
import com.alexpetitjean.timeouttile.temporaryTimeoutValueKey
import com.alexpetitjean.timeouttile.ui.components.LargeSpacer
import com.alexpetitjean.timeouttile.ui.components.MediumSpacer
import com.alexpetitjean.timeouttile.ui.components.SmallSpacer
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.io.File

@Composable
fun MainScreen(
    modifier: Modifier = Modifier,
    dataStore: DataStore<Preferences>,
    launchPermissionManagement: () -> Unit,
    canWrite: () -> Boolean,
    getCurrentTimeoutValue: () -> Int,
) {
    ConstraintLayout(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = 32.dp),
    ) {
        val (edit, permissions) = createRefs()

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .constrainAs(edit) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                    bottom.linkTo(permissions.top)
                },
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            val originalTimeoutMinutesState = dataStore.data
                .map { it[originalTimeoutValueKey] ?: 120_000 }
                .map { it / 1000 / 60 }
                .map { it.toString() }
                .collectAsState(initial = "")

            val temporaryTimeoutMinutesState = dataStore.data
                .map { it[temporaryTimeoutValueKey] ?: DEFAULT_TEMPORARY_TIMEOUT }
                .map { it / 1000 / 60 }
                .map { it.toString() }
                .collectAsState(initial = "")

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                TimeoutValueDisplay(
                    label = stringResource(R.string.main_lbl_active),
                    timeout = temporaryTimeoutMinutesState.value,
                )
                TimeoutValueDisplay(
                    label = stringResource(R.string.main_lbl_inactive),
                    timeout = originalTimeoutMinutesState.value,
                )
            }

            MediumSpacer()

            val dialogVisibleState = remember { mutableStateOf(false) }
            EditDialog(dialogVisibleState, dataStore)

            Button(
                onClick = { dialogVisibleState.value = true }
            ) {
                Text(stringResource(R.string.main_btn_edit_timeout))
            }

            LargeSpacer()

            Text(
                text = stringResource(R.string.main_text_settings_disclaimer),
                style = MaterialTheme.typography.caption,
            )
        }

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 32.dp)
                .constrainAs(permissions) {
                    bottom.linkTo(parent.bottom)
                },
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            var canWriteState by remember { mutableStateOf(canWrite()) }
            var currentTimeoutValue by remember { mutableStateOf(getCurrentTimeoutValue()) }

            LaunchedEffect(canWriteState, currentTimeoutValue) {
                launch {
                    while (true) {
                        canWriteState = canWrite()
                        delay(2000)
                    }
                }

                launch {
                    while (true) {
                        currentTimeoutValue = getCurrentTimeoutValue()
                        delay(2000)
                    }
                }
            }

            PermissionStatusText(canWriteState)

            MediumSpacer()

            Button(
                enabled = !canWriteState,
                onClick = {
                    launchPermissionManagement()
                }
            ) {
                Text(stringResource(R.string.main_btn_grant_permissions))
            }
        }
    }
}

@Composable
private fun TimeoutValueDisplay(label: String, timeout: String) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = label,
            style = MaterialTheme.typography.h6,
            textAlign = TextAlign.Center,
        )
        Text(
            text = timeout,
            style = MaterialTheme.typography.h1,
            textAlign = TextAlign.Center,
            fontFamily = FontFamily.Monospace,
        )
    }
}

@Composable
private fun PermissionStatusText(canWriteState: Boolean) {
    if (canWriteState) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Icon(
                imageVector = Icons.Default.Check,
                contentDescription = null,
            )
            Text(
                text = stringResource(R.string.main_text_permissions_granted),
                modifier = Modifier.padding(start = 8.dp)
            )
        }
    } else {
        Text(stringResource(R.string.main_text_permissions_not_granted))
    }
}

@Composable
private fun EditDialog(
    dialogVisibleState: MutableState<Boolean>,
    dataStore: DataStore<Preferences>,
) {
    val scope = rememberCoroutineScope()

    val originalTimeoutValue = remember { mutableStateOf(TextFieldValue()) }
    LaunchedEffect(scope) {
        val valueMillis = dataStore.data.first()[originalTimeoutValueKey] ?: 120_000
        val valueMinutes = valueMillis / 1000 / 60
        originalTimeoutValue.value = TextFieldValue(
            text = valueMinutes.toString(),
            selection = TextRange(valueMinutes.toString().length)
        )
    }

    val temporaryTimeoutValue = remember { mutableStateOf(TextFieldValue()) }
    LaunchedEffect(scope) {
        val valueMillis = dataStore.data.first()[temporaryTimeoutValueKey] ?: 120_000
        val valueMinutes = valueMillis / 1000 / 60
        temporaryTimeoutValue.value = TextFieldValue(
            text = valueMinutes.toString(),
            selection = TextRange(valueMinutes.toString().length)
        )
    }

    if (dialogVisibleState.value) {
        AlertDialog(
            onDismissRequest = { dialogVisibleState.value = false },
            confirmButton = {
                TextButton(
                    onClick = {
                        scope.launch {
                            dataStore.edit { prefs ->
                                prefs[originalTimeoutValueKey] = originalTimeoutValue.value.text
                                    .replace(
                                        Regex.fromLiteral("\\D"),
                                        ""
                                    )
                                    .toIntOrNull()
                                    .let { it ?: 0 }
                                    .let { it * 60 * 1000 }

                                prefs[temporaryTimeoutValueKey] = temporaryTimeoutValue.value.text
                                    .replace(
                                        Regex.fromLiteral("\\D"),
                                        ""
                                    )
                                    .toIntOrNull()
                                    .let { it ?: 0 }
                                    .let { it * 60 * 1000 }
                            }
                        }

                        dialogVisibleState.value = false
                    }
                ) {
                    Text(stringResource(R.string.all_save))
                }
            },
            dismissButton = {
                TextButton(
                    onClick = { dialogVisibleState.value = false }
                ) {
                    Text(stringResource(R.string.all_cancel))
                }
            },
            title = {
                Text(stringResource(R.string.main_edit_dialog_title))
                MediumSpacer()
            },
            text = {
                Column {
                    OutlinedTextField(
                        value = originalTimeoutValue.value,
                        onValueChange = {
                            originalTimeoutValue.value = it
                        },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    )

                    SmallSpacer()

                    OutlinedTextField(
                        value = temporaryTimeoutValue.value,
                        onValueChange = {
                            temporaryTimeoutValue.value = it
                        },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    )
                }
            },
        )
    }
}

// region Previews

@Preview(showBackground = true)
@Composable
fun MainScreenPreview() {
    MainScreen(
        dataStore = FakePreferenceDataStoreProvider.provide(),
        launchPermissionManagement = {},
        canWrite = { false },
        getCurrentTimeoutValue = { -1 }
    )
}

@Preview(showBackground = true)
@Composable
fun MainScreenPreview_PermissionGranted() {
    MainScreen(
        dataStore = FakePreferenceDataStoreProvider.provide(),
        launchPermissionManagement = {},
        canWrite = { true },
        getCurrentTimeoutValue = { 120000 }
    )
}

private object FakePreferenceDataStoreProvider {
    fun provide(): DataStore<Preferences> =
        PreferenceDataStoreFactory.create {
            File("fake.preferences_pb")
        }
}

@Preview(showBackground = true)
@Composable
fun EditDialogPreview() {
    EditDialog(
        dialogVisibleState = remember { mutableStateOf(true) },
        dataStore = FakePreferenceDataStoreProvider.provide(),
    )
}

@Preview(showBackground = true, widthDp = 400)
@Composable
fun TimeoutValueDisplayPreview() {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        TimeoutValueDisplay(
            label = "Active",
            timeout = "10",
        )
        TimeoutValueDisplay(
            label = "Inactive",
            timeout = "2",
        )
    }
}

// endregion Previews
